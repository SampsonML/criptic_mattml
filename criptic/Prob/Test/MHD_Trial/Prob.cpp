
#include <AMReX_Vector.H>
#include <AMReX_Particles.H>
#include "Prob.H"
#include "Constants.H"
#include "Gas.H"
#include "SR.H"
#include <math.h> 

// HDF5 things
#include <string>
#include <new>
#include "hdf5.h"
#include <fstream>
#include <sstream>
#include <vector>
#include "H5Cpp.h"
#include <experimental/iterator>
#include "FlashGG_mpi.h" /// Flash General Grid class
#ifndef H5_NO_NAMESPACE
    using namespace H5;
#endif


//////////////////////////////////////////////////////////////////////
// Christophs function to interpolate
//////////////////////////////////////////////////////////////////////
// === get_uni_grid_from_flash ===
// args: np is a 3D vector with the number of cells requested for the uni grid
// args: bounds is a 3D x 2 vector with again, x, y, z (as np),
//       but 2nd index being low and high edge of requested uni grid coordinate
float * get_uni_grid_from_flash(std::vector<int> np,
                                std::vector< std::vector<double> > bounds,
                                std::string hdf5filename, std::string hdf5datasetname,
                                bool mass_weighting, class FlashGG gg)
{
    enum {X, Y, Z};
    static bool const Debug = false;

    if (Debug) std::cout << "get_uni_grid_from_flash: entering..." << std::endl;

    // open the FLASH GG file
    if (Debug) std::cout << "get_uni_grid_from_flash: generating FlashGG..." << std::endl;
    //FlashGG gg = FlashGG(File_Name);

    // see if the Flash sim was done with periodic boundary conditions (PBCs)
    bool periodic = true;
    //std::map<std::string, std::string> str_parms = gg.ReadStringParameters();
    // PBCs are currently only supported for all directions, so only check xl_boundary_type
    //if (str_parms.at("xl_boundary_type") == "periodic") periodic = true;
    if (Debug) std::cout<<"get_uni_grid_from_flash: periodic = "<<periodic<<std::endl;

    // call FlashGG GetUniformGrid and assign to output grid 'grid_out'
    float * grid_out = gg.GetUniformGrid(np, bounds, hdf5datasetname, mass_weighting, periodic);

    if (Debug) std::cout << "...get_uni_grid_from_flash: exiting." << std::endl;

    return grid_out;

} // end
//////////////////////////////////////////////////////////////////////

using namespace amrex;
namespace GIdx = GasIdx;


//////////////////////////////////////////////////////////////////////
// Output options and debug flags
//////////////////////////////////////////////////////////////////////
// Debug flag
static bool const debug = false;

// Print output flag
static bool const verbose = true;

// Define X Y and Z indices
enum {X, Y, Z,};

//////////////////////////////////////////////////////////////////////
// Routine to initialize the gas state
//////////////////////////////////////////////////////////////////////
void init_gas(Gas& gas, 
	      Geometry const& geom,
	      ParmParse& pp) {

  ////////////////////////////////////////////////////////////////////
  // MHD Switch
  ////////////////////////////////////////////////////////////////////
  /** If choose to load initial gas state 
   * from HDF5 sim data this willl be run
   * before defining the density,velocity and
   * magnetic fields **/
  // Switch for HDF5 loading
  bool MHD_Load;
  std::string fn;
  pp.get("MHD_Load", MHD_Load);
  ////////////////////////////////////////////////////////////////////

  // No-op to prevent warning
  (void) geom;

  // Extract the density, velocity, and magnetic field from input deck
  Real rho, chi, B_Phi;
  Vector<Real>  v(AMREX_SPACEDIM);
  pp.get("rho", rho);
  pp.get("chi", chi); // From CRIPTIC.in
  pp.queryarr("v", v);
  pp.get("B_Phi", B_Phi);

  /** Read all this data from HDF5 file **/
  // Defining all fields
  std::string Density = "dens"; std::string VX = "velx";
  std::string VY = "vely"; std::string VZ = "velz";
  std::string MX = "magx";std::string MY = "magy";
  std::string MZ = "magz";

   // Extract physical coordinates from AMREX Geometry
  const Real*dx = geom.CellSize();
  const Real*plo = geom.ProbLo(); // Full box bottom corner
  const Real*phi = geom.ProbHi(); // Full box top corner

  // Define gg class for input file
  // Load the filename in
  pp.get("Filename", fn);
  FlashGG gg = FlashGG(fn);
  // Testing print info
  gg.PrintInfo();

  // Checks for accuracy of mapping
  int NBLK = gg.GetNumBlocks_PB();
  std::vector<int> N = gg.GetN();
  int FLASH_x_cells = N[X];
  int FLASH_y_cells = N[Y];
  int FLASH_z_cells = N[Z];
  
  Real xcells, ycells, zcells;
  pp.get("xcells", xcells);
  pp.get("ycells", ycells);
  pp.get("zcells", zcells);
  // Declare variable for total CRIPTIC blocks
  double CRIPTIC_Blocks;
  double Total_cells = xcells*ycells*zcells;

  // Soft warning about issues with cell number choices - won't kill exe
  std::cout<< "FLASH x y z cell nums: "<< FLASH_x_cells << " " << FLASH_y_cells << " " << FLASH_z_cells << std::endl;
  std::cout<< "CRIPTIC x y z cell nums: " << xcells << " " << ycells << " " << zcells << std::endl;
  std::cout<< "Miss-matching cell nums could cause interpolation errors on block boundaries - untrustworthy results" <<std::endl;
  

// Track progress
int block_counter = 1;
  // Loop over boxes - Turnoff multithreading for data read in
/**
#ifdef _OPENMP
#pragma omp parallel if (Gpu::notInLaunchRegion())
#endif
**/
  for (MFIter mfi(gas,TilingIfNotGPU()); mfi.isValid(); ++mfi) {

    // Grab the box on which to operate
    // Box in AMREX represents cartesian box
    const Box& bx = mfi.tilebox();  // Grabs local AMREX tile
    
    // Get the corners of the CRIPTIC block
    const int*low_Corner  =  bx.loVect(	);
    const int*hi_Corner  =  bx.hiVect( );

    // Get array for data , whats this data
    Array4<Real> const& data = gas[mfi].array();

    // Defining the real values faces/bounds for the CRIPTIC block
    std::vector<double> CRIPTIC_Domain_L;
    pp.queryarr("L", CRIPTIC_Domain_L);
    Real x = (plo[X] + (low_Corner[X]*dx[X])) / (2*CRIPTIC_Domain_L[X]);
    Real y = (plo[Y] + (low_Corner[Y]*dx[Y])) / (2*CRIPTIC_Domain_L[Y]);
    Real z = (plo[Z] + (low_Corner[Z]*dx[Z]))  / (2*CRIPTIC_Domain_L[Z]);
    Real x1 = (plo[X] + ((1 + hi_Corner[X])*dx[X]))  / (2*CRIPTIC_Domain_L[X]);
    Real y1 = (plo[Y] + ((1 + hi_Corner[Y])*dx[Y]))  / (2*CRIPTIC_Domain_L[Y]);
    Real z1 = (plo[Z] + ((1 + hi_Corner[Z])*dx[Z]))  / (2*CRIPTIC_Domain_L[Z]);
    // Vector of bounds
    std::vector< std::vector<double> > bounds = { {x , x1} , {y, y1} , {z, z1} };

    // Searching for segfault
    if (verbose) std::cout<< "x low: " << x << " x high: " << x1 << " Block length in x: " << x1 - x << " times L" << std::endl;
    if (verbose) std::cout<< "y low: " << y << " y high: " << y1 << " Block length in y: " << y1 - y << " times L" << std::endl;
    if (verbose) std::cout<< "z low: " << z << " z high: " << z1 << " Block length in z: " << z1 - z << " times L" << std::endl;
    
    // Define the cell numbers in the specific block
    int nx = 1 + hi_Corner[X] - low_Corner[X];
    int ny = 1 + hi_Corner[Y] - low_Corner[Y];
    int nz = 1 + hi_Corner[Z] - low_Corner[Z]; 
    // Defining cell count vector for Block
    std::vector<int> np = {nx,ny,nz};

    if (debug) std::cout<< "Cells in x: " << np[X] << std::endl;
    if (debug) std::cout<< "Cells in y: " << np[Y] << std::endl;
    if (debug) std::cout<< "Cells in z: " << np[Z] << std::endl;

    // Mass weighting option
    bool mass_weighting = false;
  
    if (debug) std::cout<< "Before uni-grid call" << std::endl;
    // Run uniform grid interpolation code (from Christoph)
    float * Grid_Block_dens = get_uni_grid_from_flash(np,bounds,fn,Density,mass_weighting,gg); 
    float * Grid_Block_velx = get_uni_grid_from_flash(np,bounds, fn,VX, mass_weighting,gg);
    float * Grid_Block_vely = get_uni_grid_from_flash(np,bounds, fn,VY, mass_weighting,gg); 
    float * Grid_Block_velz = get_uni_grid_from_flash(np,bounds, fn,VZ, mass_weighting,gg); 
    float * Grid_Block_magx = get_uni_grid_from_flash(np,bounds, fn,MX, mass_weighting,gg); 
    float * Grid_Block_magy = get_uni_grid_from_flash(np,bounds, fn,MY, mass_weighting,gg); 
    float * Grid_Block_magz = get_uni_grid_from_flash(np,bounds, fn,MZ, mass_weighting,gg); 
     
    if (block_counter < 2) CRIPTIC_Blocks = Total_cells / (np[X]*np[Y]*np[Z]);
    // Update counter
    if (verbose) std::cout<< "Mapping FLASH to CRIPTIC data for block: " << block_counter << " of " << CRIPTIC_Blocks << std::endl;
    if (block_counter == CRIPTIC_Blocks) std::cout << "All FLASH data read into CRIPTIC blocks" << std::endl;
    block_counter++;
    
    ////////////////////////////////////////////////////////////////////
    // The main data filling MHD/FLASH loop
    ////////////////////////////////////////////////////////////////////
    if (MHD_Load) {
      
      // Initialize data --- What are i, j and k here specifically???
      ParallelFor(bx, [=] AMREX_GPU_DEVICE (int i, int j, int k) {
      // Loops over cells of tilebox
      // Map the FLASH fields to the CRIPTIC block
      // Mapping to correct indices
      long NBXY = np[X]*np[Y];
      long cellIdx = (k % np[Z])*NBXY + (j % np[Y])*np[X] + i;

      // Map the data
      data(i,j,k,GIdx::TotalDensity) = Grid_Block_dens[cellIdx];
      data(i,j,k,GIdx::IonDensity) = Grid_Block_dens[cellIdx] * chi;
      data(i,j,k,GIdx::Vx) = Grid_Block_velx[cellIdx]; 
      data(i,j,k,GIdx::Bx) = Grid_Block_magx[cellIdx];


#if AMREX_SPACEDIM > 1
	      data(i,j,k,GIdx::Vy) = Grid_Block_vely[cellIdx]; 
	      data(i,j,k,GIdx::By) = Grid_Block_magy[cellIdx];
#endif
#if AMREX_SPACEDIM > 2
	      data(i,j,k,GIdx::Vz) = Grid_Block_velz[cellIdx]; 
	      data(i,j,k,GIdx::Bz) = Grid_Block_magz[cellIdx];
#endif
    
    });
    };
  };
  };
////////////////////////////////////////////////////////////////////
// End of Matt's Edits
////////////////////////////////////////////////////////////////////
// Note if MHD_Load = false, the code wont work, not sure the 
// nicest way to have it not load in the FLASH data and read direct 
// inputs for the data fields maybe a second seperate loop?
//  Maybe a goto call? 


//////////////////////////////////////////////////////////////////////
// Routine to initialize CR sources
//////////////////////////////////////////////////////////////////////
void init_sources(CRSourceContainer &sources,
		  ParmParse &pp,
		  ParmParse &pp_cr) {

  // Initialize CR sources; note that we initialize only one one
  // processor, then call redistribute to scatter the particles to the
  // right processor
  if (ParallelDescriptor::MyProc() == 
      ParallelDescriptor::IOProcessorNumber()) {

    // Read list of sources from parameter file
    Vector<Real> x, y, z, L, q, eMin, eMax;
    pp.queryarr("source_x", x);
    pp.queryarr("source_y", y);
    pp.queryarr("source_z", z);
    pp.queryarr("source_L", L);
    pp.queryarr("source_q", q);
    pp.queryarr("source_e_min", eMin);
    pp.queryarr("source_e_max", eMax);

    // Consistency check
    if (x.size() != y.size() ||
	x.size() != z.size() ||
	x.size() != L.size() ||
	x.size() != q.size() ||
	x.size() != eMin.size() ||
	x.size() != eMax.size()) {
      Abort("Found inconsistent number of sources!");
    }

    // Loop over sources
    for (Vector<Real>::size_type i=0; i<L.size(); i++) {

      Vector<Real> pos;
      pos.push_back(x[i]);
#if AMREX_SPACEDIM > 1
      pos.push_back(y[i]);
#endif
#if AMREX_SPACEDIM > 2
      pos.push_back(z[i]);
#endif

      // Create a source
      CRSource s(pos, L[i], eMin[i], eMax[i], q[i]);

      // Add to container at level 0, grid 0, tile 0
      std::pair<int,int> key {0,0};
      auto& particle_tile = sources.GetParticles(0)[key];
      particle_tile.push_back(s);
    }

  }

  // Distribute particles to processors
  sources.Redistribute();

}

