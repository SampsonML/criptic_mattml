#include <AMReX_Vector.H>
#include <AMReX_Particles.H>
#include "Prob.H"
#include "Constants.H"
#include "Gas.H"
#include "SR.H"
#include <math.h>

// HDF5 things
#include <string>
#include <new>
#include "hdf5.h"
#include <fstream>
#include <sstream>
#include <vector>
#include "H5Cpp.h"
#include <experimental/iterator>
#include "mpi.h"
#include "FlashGG_mpi.h" /// Flash General Grid class
#ifndef H5_NO_NAMESPACE
    using namespace H5;
#endif


//////////////////////////////////////////////////////////////////////
// Get FLASH Block Outputs
//////////////////////////////////////////////////////////////////////
// constants
#define NDIM 3
enum {X, Y, Z};
static const bool Debug = false;
static const int MAX_NUM_BINS = 10048;
static const double k_b = 1.380649e-16;
static const double m_p = 1.67262192369e-24;
static const double mu = 2.0;

// MPI stuff
int MyPE = 0, NPE = 1;

void FLASH_fields(std::string inputfile){
/** Function to read in the flash data and then extract the
 * cartesian coordinates of the block data so they may be mapped into the
 * new domain decomposition of CRIPTIC **/

    /// FLASH file meta data
    FlashGG gg = FlashGG(inputfile);
    std::vector<int> N = gg.GetN();

    /// decompose domain in blocks
    std::vector<int> MyBlocks = gg.GetMyBlocks(MyPE, NPE);

    // make sure this is a cube where N[X] = N[Y] = N[Z]
    assert (N[X] == N[Y] && N[X] == N[Z]);

    // get meta data
    int NBLK = gg.GetNumBlocks_PB();
    std::vector<int> NB = gg.GetNumCellsInBlock_PB();
    long NBXYZ = NB[X]*NB[Y]*NB[Z];
    std::vector< std::vector <std::vector<double> > > BB = gg.GetBoundingBox_PB();
    std::vector< std::vector<double> > MinMaxDomain = gg.GetMinMaxDomain();
                                              [ Read 301 lines ]
^G Get Help	  ^O WriteOut       ^R Read File      ^Y Prev Page	^K Cut Text	  ^C Cur Pos
^X Exit           ^J Justify        ^W Where Is       ^V Next Page	^U UnCut Text     ^T To Spell
