// This problem setup file initializes a uniform domain

#include <AMReX_Vector.H>
#include <AMReX_Particles.H>
#include "Prob.H"
#include "Constants.H"
#include "Gas.H"
#include "SR.H"
#include <math.h> 

// HDF5 things
#include <string>
#include <new>
#include "hdf5.h"
#include <fstream>
#include <sstream>
#include <vector>
#include "H5Cpp.h"
#include <experimental/iterator>
#include "FlashGG_mpi.h" /// Flash General Grid class
#ifndef H5_NO_NAMESPACE
    using namespace H5;
#endif

//////////////////////////////////////////////////////////////////////
// Christophs function to interpolate
//////////////////////////////////////////////////////////////////////
// === get_uni_grid_from_flash ===
// args: np is a 3D vector with the number of cells requested for the uni grid
// args: bounds is a 3D x 2 vector with again, x, y, z (as np),
//       but 2nd index being low and high edge of requested uni grid coordinate
float * get_uni_grid_from_flash(std::vector<int> np,
                                std::vector< std::vector<double> > bounds,
                                std::string hdf5filename, std::string hdf5datasetname,
                                bool mass_weighting)
{
    enum {X, Y, Z};
    static bool const Debug = false;

    if (Debug) std::cout << "get_uni_grid_from_flash: entering..." << std::endl;

    // open the FLASH GG file
    if (Debug) std::cout << "get_uni_grid_from_flash: generating FlashGG..." << std::endl;
    FlashGG gg = FlashGG(hdf5filename);

    // see if the Flash sim was done with periodic boundary conditions (PBCs)
    bool periodic = false;
    std::map<std::string, std::string> str_parms = gg.ReadStringParameters();
    // PBCs are currently only supported for all directions, so only check xl_boundary_type
    if (str_parms.at("xl_boundary_type") == "periodic") periodic = true;
    if (Debug) std::cout<<"get_uni_grid_from_flash: periodic = "<<periodic<<std::endl;

    // call FlashGG GetUniformGrid and assign to output grid 'grid_out'
    float * grid_out = gg.GetUniformGrid(np, bounds, hdf5datasetname, mass_weighting, periodic);

    if (Debug) std::cout << "...get_uni_grid_from_flash: exiting." << std::endl;

    return grid_out;

} // end


//////////////////////////////////////////////////////////////////////
// Get FLASH Block Outputs
//////////////////////////////////////////////////////////////////////
// constants
#define NDIM 3
enum {X, Y, Z};
static const bool Debug = false;
static const int MAX_NUM_BINS = 10048;
static const double k_b = 1.380649e-16;
static const double m_p = 1.67262192369e-24;
static const double mu = 2.0;

// MPI stuff
int MyPE = 1, NPE = 1;


// Function for getting block data
void FLASH_fields(std::string inputfile){
/** Function to read in the flash data and then extract the 
 * cartesian coordinates of the block data so they may be mapped into the
 * new domain decomposition of CRIPTIC **/

    /// FLASH file meta data
    FlashGG gg = FlashGG(inputfile);
    std::vector<int> N = gg.GetN();

    // make sure this is a cube where N[X] = N[Y] = N[Z]
    assert (N[X] == N[Y] && N[X] == N[Z]);

    // get meta data
    int NBLK = gg.GetNumBlocks_PB();
    std::vector<int> NB = gg.GetNumCellsInBlock_PB();
    long NBXYZ = NB[X]*NB[Y]*NB[Z];
    std::vector< std::vector <std::vector<double> > > BB = gg.GetBoundingBox_PB();
    std::vector< std::vector<double> > MinMaxDomain = gg.GetMinMaxDomain();
    std::vector< std::vector<double> > LBlock = gg.GetLBlock_PB();
    std::vector<double> L = gg.GetL();
    std::vector<double> LHalf(3); for (int dir=X; dir<=Z; dir++) LHalf[dir] = L[dir]/2.;
    std::vector<double> D = gg.GetD();

    // Testing print info
    gg.PrintInfo();

    /// decompose domain in blocks
    std::vector<int> MyBlocks = gg.GetMyBlocks(MyPE, NPE);

    /// loop over my blocks (b1)
    for (unsigned int ib=0; ib<MyBlocks.size(); ib++)
    {

      int b1 = MyBlocks[ib];
      std::cout<<" ["<<std::setw(6)<<MyPE<<"] MyBlocks[i] is "<<std::setw(6) << b1 << std::endl;
      if (MyPE==0) std::cout<<" ["<<std::setw(6)<<MyPE<<"] working on block1 = "<<std::setw(6)<<ib+1<<" of "<<MyBlocks.size()<<std::endl;
      std::vector<double> bc1(3);
      bc1[X] = (BB[b1][X][0]+BB[b1][X][1])/2.;
      bc1[Y] = (BB[b1][Y][0]+BB[b1][Y][1])/2.;
      bc1[Z] = (BB[b1][Z][0]+BB[b1][Z][1])/2.;
      double block_diagonal = sqrt( (BB[b1][X][1]-BB[b1][X][0])*(BB[b1][X][1]-BB[b1][X][0]) +
                                    (BB[b1][Y][1]-BB[b1][Y][0])*(BB[b1][Y][1]-BB[b1][Y][0]) +
                                    (BB[b1][Z][1]-BB[b1][Z][0])*(BB[b1][Z][1]-BB[b1][Z][0]) );

      /// read block data
      if (MyPE==0 && Debug) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1..."<<std::endl;
      float *dens = gg.ReadBlockVar_PB(b1, "dens");
      if (MyPE==0 && Debug && dens) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 dens done."<<std::endl;

      /// velocity
      float *velx = gg.ReadBlockVar_PB(b1, "velx");
      if (MyPE==0 && Debug && velx) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 velx done."<<std::endl;
      float *vely = gg.ReadBlockVar_PB(b1, "vely");
      if (MyPE==0 && Debug && vely) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 vely done."<<std::endl;
      float *velz = gg.ReadBlockVar_PB(b1, "velz");
      if (MyPE==0 && Debug && velz) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 velz done."<<std::endl;

      /// mag fields
      float *magx = gg.ReadBlockVar_PB(b1, "magx");
      if (MyPE==0 && Debug && magx) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 magx done."<<std::endl;
      float *magy = gg.ReadBlockVar_PB(b1, "magy");
      if (MyPE==0 && Debug && magy) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 magy done."<<std::endl;
      float *magz = gg.ReadBlockVar_PB(b1, "magz");
      if (MyPE==0 && Debug && magz) std::cout<<" ["<<std::setw(6)<<MyPE<<"] reading block1 magz done."<<std::endl;

     
    // Printing for testing
    //std::cout<< "Printing velocity vector i: " << velx[ib] << std::endl;

    };

  // Return the vectors I want



};

using namespace amrex;
namespace GIdx = GasIdx;

//////////////////////////////////////////////////////////////////////
// Routine to initialize the gas state
//////////////////////////////////////////////////////////////////////
void init_gas(Gas& gas, 
	      Geometry const& geom,
	      ParmParse& pp) {

  ////////////////////////////////////////////////////////////////////
  // MHD Switch
  ////////////////////////////////////////////////////////////////////
  /** If choose to load initial gas state 
   * from HDF5 sim data this willl be run
   * before defining the density,velocity and
   * magnetic fields **/
  // Switch for HDF5 loading
  bool MHD_Load;
  std::string fn;
  pp.get("MHD_Load", MHD_Load);
  /**
  int NBLK;
  pp.get("Filename", fn);
  FLASH_fields(fn);
  FlashGG gg = FlashGG(fn);
  std::vector<int> MyBlocks;
  // Load in global variables for MHD main loop
  if (MHD_Load) {
      // Filename fron criptic input file
      NBLK = gg.GetNumBlocks_PB(); // MBLK block to loop over
      std::cout << "Number of Blocks in FLASH File is: " << NBLK << std::endl;
      unsigned int num_block = 1;
      std::cout<< "Test BEFORE" << std::endl;
      //FLASH_coords(fn, num_block);
  };**/
  ////////////////////////////////////////////////////////////////////

  // No-op to prevent warning
  (void) geom;

  // Extract the density, velocity, and magnetic field from input deck
  Real rho, chi, B_Phi;
  Vector<Real>  v(AMREX_SPACEDIM);
  pp.get("rho", rho);
  pp.get("chi", chi); // From CRIPTIC.in
  pp.queryarr("v", v);
  pp.get("B_Phi", B_Phi);

  /** Read all this data from HDF5 file **/
  

   // Extract physical coordinates from AMREX Geometry
  const Real*dx = geom.CellSize();
  const Real*plo = geom.ProbLo();
  const Real*phi = geom.ProbHi();
  
  // Loop over boxes
#ifdef _OPENMP
#pragma omp parallel if (Gpu::notInLaunchRegion())
#endif
  for (MFIter mfi(gas,TilingIfNotGPU()); mfi.isValid(); ++mfi) {

    // Grab the box on which to operate
    // Box in AMREX represents cartesian box
    const Box& bx = mfi.tilebox();  // Grabs local AMREX tile
    
    // Get the corners of the CRIPTIC block
    const int*low_Corner  =  bx.loVect(	);
    const int*hi_Corner  =  bx.hiVect( );

    // Get pointer to data , whats this data
    Array4<Real> const& data = gas[mfi].array();

    ////////////////////////////////////////////////////////////////////
    // The main MHD/FLASH loop
    ////////////////////////////////////////////////////////////////////
    // Expand this loop to contain all the FLASH things

    if (MHD_Load) {
      
      // Load the filename in
      pp.get("Filename", fn);

      // Initialize data --- What are i, j and k here specifically???
      ParallelFor(bx, [=] AMREX_GPU_DEVICE (int i, int j, int k) {
      // Loops over cells of tilebox
      // get x,y,z of cell, check if in FLASH
      // if so map to data

      // Define criptic domain
      std::vector<Real> CRIPTIC_Domain_L;
      pp.queryarr("xhi", CRIPTIC_Domain_L);

      // Defining the real values faces/bounds for the CRIPTIC block
      Real x = (plo[0] + (low_Corner[0]*dx[0])) / 2*CRIPTIC_Domain_L[0];
      Real y = (plo[1] + (low_Corner[1]*dx[1])) / 2*CRIPTIC_Domain_L[1];
      Real z = (plo[2] + (low_Corner[2]*dx[2])) / 2*CRIPTIC_Domain_L[2];
      Real x1 = (plo[0] + ((1 + low_Corner[0])*dx[0])) / 2*CRIPTIC_Domain_L[0];
      Real y1 = (plo[1] + ((1 + low_Corner[1])*dx[1])) / 2*CRIPTIC_Domain_L[1];
      Real z1 = (plo[2] + ((1 + low_Corner[2])*dx[2])) / 2*CRIPTIC_Domain_L[2];

      // Vector of bounds
      std::vector< std::vector<double> > bounds = { {x , x1} , {y, y1} , {z, z1} };

      // Define the cell numbers in the specific block
      int nx = hi_Corner[0] - low_Corner[0] + 1;
      int ny = hi_Corner[1] - low_Corner[1] + 1;
      int nz = hi_Corner[2] - low_Corner[2] + 1;

      // Defining cell count vector for Block
      std::vector<int> np = {nx,ny,nz};
      // Mass weighting option
      bool mass_weighting = false;

      // Defining all fields
      std::string Density = "dens"; std::string VX = "velx";
      std::string VY = "vely"; std::string VZ = "velz";
      std::string MX = "magx";std::string MY = "magy";
      std::string MZ = "magz";

      // Run uniform grid interpolation code (from Christoph)
      float * Grid_Block_dens = get_uni_grid_from_flash(np,bounds,fn,Density,mass_weighting); 
      float * Grid_Block_velx = get_uni_grid_from_flash(np,bounds, fn,VX, mass_weighting);
      float * Grid_Block_vely = get_uni_grid_from_flash(np,bounds, fn,VY, mass_weighting); 
      float * Grid_Block_velz = get_uni_grid_from_flash(np,bounds, fn,VZ, mass_weighting); 
      float * Grid_Block_magx = get_uni_grid_from_flash(np,bounds, fn,MX, mass_weighting); 
      float * Grid_Block_magy = get_uni_grid_from_flash(np,bounds, fn,MY, mass_weighting); 
      float * Grid_Block_magz = get_uni_grid_from_flash(np,bounds, fn,MZ, mass_weighting); 

      // De-reference memory address
      std::cout<< "Output grid uniform: " << Grid_Block_dens << std::endl;

      // Map the FLASH fields to the CRIPTIC block
      data(i,j,k,GIdx::TotalDensity) = 1; // Grid_Block_dens[i][j][k];
      data(i,j,k,GIdx::IonDensity) = 1; //Grid_Block_dens[i][j][k] * chi;
      data(i,j,k,GIdx::Vx) = 1; //Grid_Block_velx[i][j][k]; 
      data(i,j,k,GIdx::Bx) = 1; //Grid_Block_magx[i][j][k];


#if AMREX_SPACEDIM > 1
	      data(i,j,k,GIdx::Vy) = 1; //Grid_Block_vely[i][j][k]; 
	      data(i,j,k,GIdx::By) = 1; //Grid_Block_magy[i][j][k];
#endif
#if AMREX_SPACEDIM > 2
	      data(i,j,k,GIdx::Vz) = 1; //Grid_Block_velz[i][j][k]; 
	      data(i,j,k,GIdx::Bz) = 1; //Grid_Block_magz[i][j][k];
#endif
    
    });

    };

  };

////////////////////////////////////////////////////////////////////
// End of Matt's Edits
////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Routine to initialize CR sources
//////////////////////////////////////////////////////////////////////
void init_sources(CRSourceContainer &sources,
		  ParmParse &pp,
		  ParmParse &pp_cr) {

  // Initialize CR sources; note that we initialize only one one
  // processor, then call redistribute to scatter the particles to the
  // right processor
  if (ParallelDescriptor::MyProc() == 
      ParallelDescriptor::IOProcessorNumber()) {

    // Read list of sources from parameter file
    Vector<Real> x, y, z, L, q, eMin, eMax;
    pp.queryarr("source_x", x);
    pp.queryarr("source_y", y);
    pp.queryarr("source_z", z);
    pp.queryarr("source_L", L);
    pp.queryarr("source_q", q);
    pp.queryarr("source_e_min", eMin);
    pp.queryarr("source_e_max", eMax);

    // Consistency check
    if (x.size() != y.size() ||
	x.size() != z.size() ||
	x.size() != L.size() ||
	x.size() != q.size() ||
	x.size() != eMin.size() ||
	x.size() != eMax.size()) {
      Abort("Found inconsistent number of sources!");
    }

    // Loop over sources
    for (Vector<Real>::size_type i=0; i<L.size(); i++) {

      Vector<Real> pos;
      pos.push_back(x[i]);
#if AMREX_SPACEDIM > 1
      pos.push_back(y[i]);
#endif
#if AMREX_SPACEDIM > 2
      pos.push_back(z[i]);
#endif

      // Create a source
      CRSource s(pos, L[i], eMin[i], eMax[i], q[i]);

      // Add to container at level 0, grid 0, tile 0
      std::pair<int,int> key {0,0};
      auto& particle_tile = sources.GetParticles(0)[key];
      particle_tile.push_back(s);
    }

  }

  // Distribute particles to processors
  sources.Redistribute();

}
