# README #

Matt Sampsons version of CRIPTIC with AMREX.
Ability to read in FLASH hdf5 files has been included to be used for the initial gas background. 

### Requirements over Standard CRIPTIC ###
This will require the inclusion and PATH linking to a valid hdf5 library. 
Ideally hdf5 version 1.8.21 or higher compiled with C++ compatability.

### FLASH Use ###
Simply add an input hdf5 file to the same directory as Prob.cpp
Edit criptic.in so that prob.Filename is the same as the name of the hdf5 file.
In criptic.in ensure the number of cells matches the number of cells in the
FLASH simulation otherwise numerical errors may occur due to an incorrect
interpolation of FLASH gas background to CRIPTIC gas background.

### Contact ###
Contact Matt Sampson with any questions.
Email: u6847483@anu.edu.au